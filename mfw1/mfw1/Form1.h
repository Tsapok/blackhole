#pragma once
namespace mfw1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace std;


	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  num1;
	protected: 
	private: System::Windows::Forms::TextBox^  num2;
	private: System::Windows::Forms::Button^  button1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->num1 = (gcnew System::Windows::Forms::TextBox());
			this->num2 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// num1
			// 
			this->num1->Location = System::Drawing::Point(134, 106);
			this->num1->Name = L"num1";
			this->num1->Size = System::Drawing::Size(100, 20);
			this->num1->TabIndex = 0;
			// 
			// num2
			// 
			this->num2->Location = System::Drawing::Point(134, 154);
			this->num2->Name = L"num2";
			this->num2->Size = System::Drawing::Size(100, 20);
			this->num2->TabIndex = 1;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(134, 217);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(100, 23);
			this->button1->TabIndex = 2;
			this->button1->Text = L"button1";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(393, 291);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->num2);
			this->Controls->Add(this->num1);
			this->Name = L"Form1";
			this->Text = L"Form1";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e)
			 {

				 double number= System::Convert::ToDouble(num1->Text);
				 number=number*3;
				 num2->Text=System::Convert::ToString(number);
				 
			 }
	};
}

